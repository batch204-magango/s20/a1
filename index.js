// console.log("Hi Maam Mia");


//3. Create a variable number that will store the value of the number provided by the user via the prompt.

let number = Number(prompt("Enter a number:"));
console.log("The number you provided is " + number + ".");

// Create a condition that if the current value is less than or equal to 50,stop the loop.

for(number; number > 0; number--){
	if(number <= 50){
		break;
	}

// Create another condition that if the current value is divisible by 10,print a message that the number is being skipped and continue tothe next iteration of the loop.


	if(number % 10 === 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

// Create another condition that if the current value is divisible by 5,print the number.
	if(number % 5 === 0){
		console.log(number);
	}
}
console.log("The current value is at " + number + ". Terminating the loop.");


//Create a variable that will contain the stringsupercalifragilisticexpialidocious.

let word = "supercalifragilisticexpialidocious";

// Create another variable that will store the consonants from the string.
let wordWithoutVowels = "";


//Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.

for(let i = 0; i <word.length; i++)
{
	if(	word[i] === "a" ||
		word[i] === "e" ||
		word[i] === "i" ||
		word[i] === "o" ||
		word[i] === "u"){
		continue;
	}
//Create an else statement that will add the letter to the second variable.
	else{
		wordWithoutVowels += word[i];
	}
}
console.log("");
console.log(word);
console.log(wordWithoutVowels);//
